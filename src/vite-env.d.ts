/// <reference types="vite/client" />

type ArchiveItem = {
	files: {
		crc32: string;
		format: string;
		md5: string;
		mtime: string;
		name: string;
		sha1: string;
		size: string;
		source: string;
	}[],
	metadata: Record<string, string | string[]>;
	reviews: {
			createdate: string;
			review_id: string;
			reviewbody: string;
			reviewdate: string;
			reviewer: string;
			reviewtitle: string;
			stars: string;
	}[];
};

type RelatedItem = {
	_score: number;
	_id: string;
	_source: {
		title: string[];
	};
};
