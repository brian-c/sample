import { css } from 'lit';

// TODO: This is kinda weird, but maybe an okay way to share table styles?
// I suspect there's a way to do something like <table is="data-table"> though.

export default css`
	.data-table {
		border-collapse: collapse;
		border-spacing: 0;
		width: 100%;
	}

	.data-table > thead > tr > th {
		border-bottom: 1px solid #8883;
		font-weight: normal;
	}

	.data-table > tbody > tr:nth-child(even) {
		background: #8881;
	}

	.data-table > tbody > tr > :not(:first-child) {
		border-inline-start: 1px solid #8883;
	}

	.data-table > * > tr > th,
	.data-table > * > tr > td {
		padding: 0.5vw 2vw;
		text-align: start;
	}
`
