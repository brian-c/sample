import { customElement, property, query, state } from 'lit/decorators.js';
import { css, html, LitElement } from 'lit';
import '@fontsource/libre-caslon-text/400.css';
import '@fontsource/libre-caslon-text/400-italic.css';

@customElement('resizable-frame')
export class ResizableFrame extends LitElement {
	@property({ reflect: true })
	src: HTMLIFrameElement['src'] = '';

	@query('iframe')
	iframe!: HTMLIFrameElement;

	@property({ attribute: 'aspect-ratio', type: Number, reflect: true })
	aspectRatio = 16 / 9;

	@state()
	thumbIsBeingDragged = false;

	lastDragY = 0;

	handleThumbDown = (event: PointerEvent) => {
		event.preventDefault();
		this.thumbIsBeingDragged = true;

		addEventListener('pointermove', this.handleThumbDrag, true);
		addEventListener('pointerup', this.handleThumbRelease);

		this.lastDragY = event.pageY;
	}

	handleThumbDrag = (event: PointerEvent) => {
		const delta = event.pageY - this.lastDragY;
		this.aspectRatio = this.iframe.offsetWidth / (this.iframe.offsetHeight + delta);

		this.lastDragY = event.pageY;
	}

	handleThumbRelease = () => {
		removeEventListener('pointermove', this.handleThumbDrag, true);
		removeEventListener('pointerup', this.handleThumbRelease);

		this.thumbIsBeingDragged = false;
	}

	handleKeyDown = (event: KeyboardEvent) => {
		let heightChange = 0;
		if (event.key === 'ArrowDown') {
			heightChange = 10;
		} else if (event.key === 'ArrowUp') {
			heightChange = -10;
		}

		if (event.shiftKey) heightChange *= 5;

		if (heightChange !== 0) {
			event.preventDefault();
			this.aspectRatio = this.iframe.offsetWidth / (this.iframe.offsetHeight + heightChange);
		}
	}

	static styles = css`
		iframe {
			aspect-ratio: var(--aspect-ratio);
			background: gray;
			border: 0;
			box-sizing: border-box;
			display: block;
			width: 100%;
		}

		#thumb {
			--base-bg: #444;
			background: repeating-linear-gradient(#0008, #0008 1px, #0004 1px, #0004 2px), var(--base-bg);
			clip-path: polygon(0 0, 100% 0, calc(100% - 1ch) 100%, 1ch 100%);
			cursor: ns-resize;
			height: 1rem;
			margin: 0 auto;
			width: 25%;
			transition: background-color 0.1s;
		}

		#thumb:hover {
			--base-bg: #555;
		}
	`;

	render() {
		return html`
			<iframe
				src="${this.src}"
				allow="fullscreen"
				style="
					--aspect-ratio: ${this.aspectRatio};
					pointer-events: ${this.thumbIsBeingDragged ? 'none' : ''};
				"
			></iframe>

			<div id="thumb"
				role="separator"
				aria-label="Aspect ratio"
				aria-valuenow="${this.aspectRatio.toFixed(2)} to 1"
				tabindex="0"
				@pointerdown="${this.handleThumbDown}"
				@keydown="${this.handleKeyDown}"
			></div>
		`;
	}
};
