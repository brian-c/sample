import { LitElement, css, html } from 'lit';
import { customElement, property } from 'lit/decorators.js';
import { makeSafeHtml } from '../lib/make-safe-html';

@customElement('reviews-list')
export class ReviewsList extends LitElement {
	@property()
	reviews = {} as ArchiveItem['reviews'];

	static styles = css`
		ul {
			list-style: none;
			margin: 0;
			padding: 0;
		}

		article {
			margin-block: 2vw;
		}

		header {
			margin-block-end: 1ch;
		}

		h3 {
			margin: 0;
		}

		.stars {
			font-size: 0.8em;
			opacity: 0.7;
			vertical-align: 0.1em;
		}

		.byline {
			font-weight: bold;
		}

		hr {
			border: 1px solid #8888;
			border-width: 1px 0 0 0;
			margin-block: 4vw;
		}
	`;

	render() {
		return html`
			<ul>
				${this.reviews.map((review, i) => html`
					<li>
						${i > 0 ? html`<hr />` : null}

						<article>
							<header>
								<h3>
									${review.reviewtitle}
									<span class="stars" aria-label="${review.stars} stars">
										${'★'.repeat(Math.round(parseFloat(review.stars)))}<!--
										-->${'☆'.repeat(5 - Math.round(parseFloat(review.stars)))}
									</span>
								</h3>

								<div class="byline">
									${review.reviewer},
									${new Date(review.reviewdate).toDateString()}
								</div>
							</header>

							<div>
								${makeSafeHtml(review.reviewbody)}
							</div>
						</article>
					</li>
				`)}
			</ul>
		`;
	}
};
