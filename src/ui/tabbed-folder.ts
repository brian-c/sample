import { customElement, property, queryAssignedElements } from 'lit/decorators.js';
import { css, html, LitElement } from 'lit';

@customElement('tabbed-folder')
export class TabbedFolder extends LitElement {
	@property({ reflect: true })
	value = '';

	@queryAssignedElements({ slot: 'tabs' })
	tabs!: HTMLAnchorElement[];

	@queryAssignedElements({ slot: 'contents' })
	contents!: HTMLElement[];

	connectedCallback() {
		super.connectedCallback();

		// TODO: How do we know when the content is ready?
		setTimeout(() => this.setUpSlottedContent());
	}

	attributeChangedCallback(name: string, old: string | null, value: string | null) {
		super.attributeChangedCallback(name, old, value);
		if (name === 'value') {
			this.syncWithValue();
		}
	}

	setUpSlottedContent() {
		this.tabs.forEach(tab => tab.setAttribute('role', 'tab'));
		this.contents.forEach(element => element.setAttribute('role', 'tabpanel'));
		this.syncWithValue();
	}

	syncWithValue = () => {
		if (this.tabs.length === 0 && this.contents.length === 0) {
			return;
		}

		this.tabs.forEach(tab => tab.setAttribute('aria-selected', String(tab.getAttribute('aria-controls') === this.value)));
		this.contents.forEach(element => element.hidden = element.id !== this.value);

		// Fall back to the first content ID.
		if (this.contents.every(element => element.hidden)) {
			this.value = this.contents[0]?.id;
		}
	};

	static styles = css`
		#wrapper {
			--_folder-color: var(--folder-color, tan);
			--_content-offset: var(--content-offset, max(1vw, 0.5rem));
			display: flex;
			flex-direction: column;
			padding-block-end: var(--_content-offset);
			padding-inline-end: var(--_content-offset);
		}

		#tabs {
			font-family: 'American Typewriter', Courier, monospace;
			font-size: large;
			margin-bottom: -1px;
			margin-top: 1px;
			overflow: auto;
			padding-inline: var(--_content-offset);
			scrollbar-width: none;
			text-shadow: 1px 1px #fff3;
			white-space: nowrap;
		}

		#tabs::-webkit-scrollbar {
			background-color: var(--_folder-color);
			height: 0.6ch;
			width: 0.6ch;
		}

		#tabs::-webkit-scrollbar-thumb {
			border-radius: 1ch;
			background: #0006;
			border: 2px solid var(--_folder-color);
		}

		#tabs > ::slotted(a) {
			background: var(--_folder-color);
			display: inline-block;
			clip-path: polygon(1ch 0, calc(100% - 1ch) 0, 100% 100%, 0 100%);
			color: inherit;
			line-height: 1.4;
			padding: 0.6ch 3ch 0.2ch;
			position: relative;
			text-decoration: none;
		}

		#tabs > ::slotted(a:not(:first-child)) {
			margin-inline-start: -1ch;
		}

		#tabs > ::slotted(a[aria-selected="true"]) {
			z-index: 1;
		}

		#tabs > ::slotted(a:not([aria-selected="true"])) {
			box-shadow: 0 1px 0 inset #fff7, 0 -1px 0 inset #fff7, 0 0 2ch inset #0005;
		}

		#contents-wrapper {
			background: var(--_folder-color);
		}

		#contents {
			inset-block-start: var(--_content-offset);
			inset-inline-start: var(--_content-offset);
			position: relative;
		}
	`;

	render() {
		return html`
			<div id="wrapper">
				<div id="tabs" role="tablist">
					<slot name="tabs"></slot>
				</div>

				<div id="contents-wrapper">
					<div id="contents">
						<slot name="contents"></slot>
					</div>
				</div>
			</div>
		`;
	}
};
