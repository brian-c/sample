import { customElement, property } from 'lit/decorators.js';
import { css, html, LitElement } from 'lit';
import patchQuery from '../lib/patch-query';

@customElement('related-item-card')
export class RelatedItemCard extends LitElement {
	@property()
	item = {} as RelatedItem;

	get thumbnailSrc() {
		return `https://archive.org/services/img/${this.item._id}`;
	}

	static styles = css`
		a {
			box-shadow: 0.1vw 0.5vw 1vw -0.5vw #0005;
			display: block;
		}

		figure {
			margin: 0;
			position: relative;
			overflow: hidden;
		}

		#thumbnail {
			--scale: 1.05; /* Try to trim off any dark edges. */
			aspect-ratio: 16 / 9;
			background: #0002;
			display: block;
			object-fit: cover;
			transform: scale(var(--scale));
			transition: transform 0.3s;
			width: 100%;
		}

		a:hover #thumbnail {
			--scale: 1.2;
		}

		figcaption {
			background: linear-gradient(#0000, #0008);
			bottom: 0;
			color: white;
			font-family: sans-serif;
			font-size: 0.8em;
			font-weight: bold;
			left: 0;
			padding: 2ch 1ch 1ch;
			position: absolute;
			right: 0;
			/* Try to make text readable on any image without obscuring too much of the image: */
			text-shadow: 0 1px 1px #000, 0 1px 3px #000, 0 2px 6px #000, 0 2px 9px #000;
		}
	`;

	render() {
		return html`
			<a href="${patchQuery({ id: this.item._id, tab: null })}">
				<figure>
					<img id="thumbnail" src="${this.thumbnailSrc}" alt="">
					<figcaption>${this.item._source.title[0]}</figcaption>
				</figure>
			</a>
		`;
	}
};
