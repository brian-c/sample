import { customElement } from 'lit/decorators.js';
import { css, html, LitElement } from 'lit';

@customElement('sheet-of-paper')
export class SheetOfPaper extends LitElement {
	static styles = css`
		#page {
			background: linear-gradient(100deg, white 75%, #eee, white);
			box-shadow: 0 1vw 2vw #0003;
			color: black;
			padding: 0.1px 0;
		}

		#content {
			margin: min(5vw, 3rem) min(6vw, 4rem) min(8vw, 6rem);
		}
	`;

	render() {
		return html`
			<div id="page">
				<div id="content">
					<slot></slot>
				</div>
			</div>
		`;
	}
};
