import { customElement, property } from 'lit/decorators.js';
import { css, html, LitElement } from 'lit';
import dataTableStyle from './data-table-style';

type MetadataEntry = [string, string | string[]];

@customElement('metadata-table')
export class MetadataTable extends LitElement {
	@property()
	metadata = {} as ArchiveItem['metadata'];

	@property()
	skip = [] as (keyof ArchiveItem['metadata'])[];

	static makeKeysNicer(key: string) {
		const SPECIAL_CASES: Record<string, string> = {
			publicdate: 'Publication date',
		};

		if (key in SPECIAL_CASES) {
			return SPECIAL_CASES[key];
		}

		const spaced = key.replace(/_/g, ' ');
		return spaced.charAt(0).toLocaleUpperCase() + spaced.slice(1);
	}

	get priorityEntries() {
		const PRIORITY_KEYS = ['collection', 'mediatype', 'publicdate'];
		const matches = Object.entries(this.metadata).filter(([key]) => PRIORITY_KEYS.includes(key));
		return this.sortEntries(matches);
	}

	get regularEntries() {
		const matches = Object.entries(this.metadata).filter(entry => !this.priorityEntries.includes(entry));
		return this.sortEntries(matches);
	}

	get allEntriesFormatted() {
		const allEntries = [...this.priorityEntries, ...this.regularEntries];
		const minusSkipped = allEntries.filter(entry => !this.skip.includes(entry[0]));
		return this.formatEntries(minusSkipped);
	}

	sortEntries(entries: MetadataEntry[]) {
		return entries.sort((a, b) => a[0] < b[0] ? -1 : a[0] > b[0] ? +1 : 0);
	}

	formatEntries(entries: MetadataEntry[]) {
		return entries.map(([key, value]) => [MetadataTable.makeKeysNicer(key), value]);
	}

	static styles = css`
		${dataTableStyle}

		td {
			word-break: break-all;
		}

		ul {
			margin: 0;
			padding-inline-start: 1em;
		}
	`;

	render() {
		return html`
			<table class="data-table">
				<tbody>
					${this.allEntriesFormatted.map(([label, values]) => html`
						<tr>
							<th scope="row">${label}</th>
							<td>
								${Array.isArray(values) ? html`
									<ul>
										${values.map(value => html`
											<li>${value}</li>
										`)}
									</ul>
								` : html`
									${values}
								`}
							</td>
						</tr>
					`)}
				</tbody>
			</table>
		`;
	}
};
