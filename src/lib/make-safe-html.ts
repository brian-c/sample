import DOMPurify from 'dompurify';
import { html } from 'lit';
import { unsafeHTML } from 'lit/directives/unsafe-html.js';

export function makeSafeHtml(markup: string | string[]) {
	const sanitized = DOMPurify.sanitize(String(markup), {
		ALLOWED_TAGS: ['a', 'b', 'br', 'em', 'i', 'strong'],
		ALLOWED_ATTR: ['href'],
	});

	return html`${(unsafeHTML(sanitized))}`;
}
