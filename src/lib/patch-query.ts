export default function patchQuery(
	patch: Record<string, string | null>,
	url = location.href
) {
	const newUrl = new URL(url);

	for (const [key, value] of Object.entries(patch)) {
		if (value === null) {
			newUrl.searchParams.delete(key);
		} else {
			newUrl.searchParams.set(key, String(value));
		}
	}

	return newUrl.href;
}
