import '@fontsource/libre-caslon-text';
import { LitElement, css, html } from 'lit';
import { customElement, state } from 'lit/decorators.js';
import { metadata as offlineMetadata, related as offlineRelated } from './lib/data-for-offline-dev';
import { makeSafeHtml } from './lib/make-safe-html';
import patchQuery from './lib/patch-query';
import dataTableStyle from './ui/data-table-style';
import './ui/metadata-table';
import './ui/related-item-card';
import './ui/resizable-frame';
import './ui/reviews-list';
import './ui/sheet-of-paper';
import './ui/tabbed-folder';

const DEFAULT_ITEM_ID = 'InformationM';
const DEFAULT_TAB = 'metadata';

@customElement('main-app')
export class MainApp extends LitElement {
	@state() itemId = '';
	@state() tab = '';
	@state() loading = 0;
	@state() item = null as ArchiveItem | null;
	@state() relatedItems = [] as RelatedItem[];
	@state() error = null as unknown;

	get embedSrc() {
		return `https://archive.org/embed/${this.itemId}`;
	}

	connectedCallback() {
		super.connectedCallback();
		addEventListener('popstate', this.handleLocationChange);
		addEventListener('click', this.overridePageClicks);
		this.handleLocationChange();
	}

	disconnectedCallback() {
		super.disconnectedCallback();
		removeEventListener('popstate', this.handleLocationChange);
		removeEventListener('click', this.overridePageClicks);
	}

	overridePageClicks = (event: MouseEvent) => {
		const targetLink = (event.composedPath()[0] as HTMLElement).closest('a');
		const samePath = targetLink?.pathname === location.pathname;
		const noModifiers = !event.altKey && !event.ctrlKey && !event.metaKey && !event.shiftKey;

		if (samePath && noModifiers) {
			event.preventDefault();
			history.pushState(null, '', targetLink.href);
			this.handleLocationChange();
		}
	};

	handleLocationChange = async () => {
		const query = new URLSearchParams(location.search);
		this.tab = query.get('tab') ?? DEFAULT_TAB;

		const itemId = query.get('id') ?? '';
		if (!itemId) {
			// Specify a default item to look at.
			const locationWithId = new URL(location.href);
			locationWithId.searchParams.set('id', DEFAULT_ITEM_ID);
			location.replace(locationWithId);
		} else if (itemId !== this.itemId) {
			this.itemId = itemId;
			await this.fetchItem(this.itemId);
		}

		document.title = `${this.item?.metadata.title ?? 'Untitled item'} (${this.tab})`;
	};

	async fetchItem(id: string) {
		this.fetchRelated(id);

		if (!navigator.onLine && import.meta.env.DEV) {
			console.warn('Offline in dev mode; using saved metadata');
			this.item = offlineMetadata as ArchiveItem;
			return;
		}

		try {
			this.error = null;
			this.loading += 1;

			const itemResponse = await fetch(`https://archive.org/metadata/${id}`);
			const item = await itemResponse.json();

			// I'm only getting 200s back from this endpoint,
			// so let's use the presence of metadata to identify a valid item.
			// TODO: Determine what's valid vs. what's invalid, and why.
			if (!item.metadata) {
				throw new Error('Invalid item');
			}

			this.item = item;
			console.info('Got item', this.item);
		} catch (error) {
			this.error = error;
		} finally {
			this.loading -= 1;
		}
	}

	async fetchRelated(id: string) {
		if (!navigator.onLine && import.meta.env.DEV) {
			console.warn('Offline in dev mode; using saved related items');
			this.relatedItems = offlineRelated.hits.hits as RelatedItem[];
			return;
		}

		try {
			this.loading += 1;
			const response = await fetch(`https://be-api.us.archive.org/mds/v1/get_related/all//${id}`);
			const data = await response.json();
			this.relatedItems = data.hits.hits as RelatedItem[];
			console.info('Got related', this.item);
		} catch (error) {
			this.relatedItems = [];
		} finally {
			this.loading -= 1;
		}
	}

	static styles = css`
		${dataTableStyle}

		main {
			--content-overlap: 10vw;
			--gap: 3vw;
			--font-serif: 'Libre Caslon Text', serif;
			font-family: var(--font-serif);
			line-height: 1.7;
		}

		#item-display {
			background: linear-gradient(#222 95%, #111);
			color: white;
			padding: 1vw var(--gap) calc(var(--content-overlap) + 1vw);
			text-shadow: 0 1px 1px #0009;
		}

		h1 {
			text-align: center;
		}

		h2 {
			align-items: center;
			display: flex;
			font-size: inherit;
			gap: 1ch;
			opacity: 0.6;
		}

		h2::before,
		h2::after {
			border-top: 3px dotted;
			content: '';
			flex-grow: 1;
			opacity: 0.4;
		}

		#description {
			border-inline: 6px dotted #8886;
			font-size: calc(1em + 0.5vw);
			margin: 0 calc(var(--gap) * 1);
			padding-inline: 4vw;
		}

		#content {
			background: #cde;
			display: flex;
			flex-wrap: wrap;
			gap: var(--gap);
			padding: 0 var(--gap) var(--gap);
		}

		#details {
			flex: 2 2 30rem;
			margin-top: calc(var(--content-overlap) * -1);
			min-width: 0;
		}

		#related {
			flex: 1 1 calc(10rem + 10vw);
		}

		.related-items-list {
			display: flex;
			flex-wrap: wrap;
			gap: 1rem;
			list-style: none;
			margin: 0;
			padding: 0;
		}

		.related-items-list > li {
			flex: 1 1 30vw;
		}

		related-item-card {
			display: block;
		}
	`;

	render() {
		if (this.loading) {
			return html`<p>Loading...</p>`;
		}

		if (this.error) {
			return html`<p>${this.error}</p>`;
		}

		if (this.item !== null) {
			const SKIPPED_FILE_FORMATS = ['Metadata', 'Thumbnail'];
			const filesToShow = this.item.files
				.filter(file => !SKIPPED_FILE_FORMATS.includes(file.format))
				.sort((a, b) => a.format.toLocaleLowerCase() < b.format.toLocaleLowerCase() ? -1 : a.format.toLocaleLowerCase() > b.format.toLocaleLowerCase() ? +1 : 0);

			const relatedItemsInOrder = this.relatedItems.sort((a, b) => b._score - a._score);

			return html`
				<main>
					<div id="item-display">
						<header id="title">
							<h1>${this.item.metadata.title ?? 'Untitled'}</h1>
						</header>

						<resizable-frame
							src="${this.embedSrc}"
							aspect-ratio="${Math.max(4 / 3, innerWidth / innerHeight / 0.8)}"
						></resizable-frame>

						${this.item.metadata.description ? html`
							<section id="description">
								<p>${makeSafeHtml(this.item.metadata.description)}</p>
							</section>
						` : null}
					</div>

					<section id="content">
						<div id="details">
							<tabbed-folder value="${this.tab}">
								<a slot="tabs" href="${patchQuery({ tab: 'metadata' })}" aria-controls="metadata">Metadata</a>
								${this.item.reviews ? html`
									<a slot="tabs" href="${patchQuery({ tab: 'reviews' })}" aria-controls="reviews">${this.item.reviews.length} Reviews</a>
								` : null}
								<a slot="tabs" href="${patchQuery({ tab: 'files' })}" aria-controls="files">Files</a>

								<sheet-of-paper slot="contents" id="metadata">
									<article>
										<h2>Metadata</h2>
										<metadata-table .metadata="${this.item.metadata}" .skip="${['title', 'description']}"></metadata-table>
										<!-- <pre>${JSON.stringify(this.item.metadata, null, 2)}</pre> -->
									</article>
								</sheet-of-paper>

								${this.item.reviews?.length > 0 ? html`
									<sheet-of-paper slot="contents" id="reviews">
										<article>
											<h2>Reviews</h2>
											<reviews-list .reviews="${this.item.reviews}"></reviews-list>
											<!-- <pre>${JSON.stringify(this.item.reviews, null, 2)}</pre> -->
										</article>
									</sheet-of-paper>
								` : null}

								<sheet-of-paper slot="contents" id="files">
									<article>
										<h2>Files</h2>

										<table class="data-table">
											<thead>
												<tr>
													<th>Format</th>
													<th>Last modified</th>
													<th>Size</th>
												</tr>
											</thead>
											<tbody>
												${filesToShow.map(file => html`
													<tr>
														<td>
															<a href="https://archive.org/download/${this.itemId}/${file.name}">${file.format}</a>
														</td>
														<td>${new Date(parseFloat(file.mtime) * 1000).toLocaleString()}</td>
														<td>${parseFloat((parseFloat(file.size) / 1024 / 1024).toFixed(2)).toLocaleString()} MiB</td>
													</tr>
												`)}
											</tbody>
										</table>
										<!-- <pre>${JSON.stringify(this.item.files, null, 2)}</pre> -->
									</article>
								</sheet-of-paper>
							</tabbed-folder>
						</div>

						${relatedItemsInOrder.length > 0 ? html`
							<aside id="related">
								<h2>${relatedItemsInOrder.length} Related Items</h2>
								<ol class="related-items-list">
									${relatedItemsInOrder.map(item => html`
										<li>
											<related-item-card .item="${item}"></related-item-card>
										</li>
									`)}
								</ol>
							</aside>
						` : null}
					</section>
				</main>
			`;
		}

		return null;
	}
};
