> **Note for Harvard/Dataverse:** I haven't had a chance to work on an open-source project in quite a while, so I'm giving you a look at a little take-home project I did for a job interview last year. The task was to create a page that connects to the Internet Archive's API and displays an item and some of its metadata. I had a lot of fun with the design on this, and I still think it's a pretty solidly organized bit of code, a year on.

Hi, thanks for taking a look at my project! I took the opportunity to give Lit a shot, since that's what you're using. I set up `main-app` component to fil the role of a server-rendered template: read the URL, fetch the data, and display it; and it also handles same-page link clicks. So it can probably be broken down a bit more. I'm curious to see how you handle that main entry point. The rest of the components mainly encapsulate styles and behaviors.

There are a couple bits I didn't make time to totally nail down, which I've marked with `TODO` comments.

You can see this project deployed at **https://bc-sample.netlify.app/**

* * *

To get this project running locally:

1. Install Node (see https://nodejs.org/en/download/)
2. Run `npm install` to install the dependencies
3. Run `npm run dev` to start the dev server
4. Visit **http://localhost:3000/** in your browser

To build and deploy:

1. Run `npm run build` to populate the **dist/** directory
2. Serve the **dist/** directory statically

* * *

Some quick design/implementation notes:

- The embedded item wants to start out with a 4:3 aspect ratio to match the initial item _InformationM_, but it also won't start any taller than the viewport. That way the entire item is visible, and the rest of the content sticking up from the bottom makes it clear that there's more on the page to scroll to.

- The embedded item is vertically resizable with a little thumb. This is so the user can set it to a comfortable size as they browse through different items. I did find two bugs after I submitted this though: sometimes the height resets when the location changes (after clicking a tab or related item), and the `aria-valuenow` attribute isn't being announced properly by VoiceOver.

- The tabbed-folder-style tabs are a little corny, I'm sorry. But I'm thinking: it's an archive, I'm digging through storage boxes and filing cabinets.

- The thumbnails for the related content don't have quite the resolution I needed here, but I figured I'd use what I had to get the idea across.

- The tabs, the thumbnails, and the spacing between everything all scale down with the viewport and wrap when it's narrow enough, the goal being to keep the content at a reasonable line length while still taking advantage of the width of the viewport.
